# prezzario_Veneto

**Prezzario Lavori Pubblici della Regione del Veneto** da utilizzare con l'estensione **LeenO** per Calc di Libre Office.
Nel repository sono presenti il prezzario completo (che non verrà aggiornato) ed i prezzari parziali suddivisi nelle categorie lavori previste nel prezzario regionale (Manodopera, Materiali, Noli, Opere edili, ecc).
I prezzari parziali saranno man mano aggiornati con le correzioni degli errori presenti in quello regionale.
Nonostante ciò ogni prezzario può contenere ancora errori.
**L'utilizzatore dovrà pertanto verificare le voci di elenco prezzi prima di inserirle nel computo. Resta sua esclusiva responsabilità la verifica dei dati caricati.**

I prezziari sono suddivisi per anno.
In ogni cartella sono disponibili i documenti della Regione del Veneto per una più facile consultazione e raffronto.

LeenO è un'estensione open source per Libre Office Calc per la redazione di computi e contabilità lavori.
La potete trovare a questo [indirizzo](https://leeno.org)
